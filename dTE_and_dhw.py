# -*- coding: utf-8 -*-
"""
Created on Thu Feb  3 23:05:40 2022

@author: Administrator
"""

import numpy as np
from Parameters import*

#This function is used to calculate dhw/dt and dTE/dt

def dhw(hw_i,TE_i,dt,b,ksi1):
    dw=(-r*hw_i-alpha*b*TE_i-alpha*ksi1)*dt
    return dw

def dTE(hw_i,TE_i,dt,b,R,ksi1,ksi2,en):
    dTE=(R*TE_i+gamma*hw_i-en*(hw_i+b*TE_i)**3+gamma*ksi1+ksi2)*dt
    return dTE

