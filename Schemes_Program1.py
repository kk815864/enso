# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 06:33:08 2022

@author: Administrator
"""
import numpy as np

from dTE_and_dhw import*
from Parameters import*


def Eular(hw,TE,dt,timesteps):
    #Two viarebles to save the hw and TE at a specific time step
    hw_i=0
    TE_i=0
    for i in range(timesteps-1):
        hw_i=hw[i]
        TE_i=TE[i]
        dhw=hw_d(hw_i,TE_i,dt,i)
        hw[i+1]=hw_i+dhw
        dTE=TE_d(hw_i,TE_i,dt,i)
        #i means time step, en may change with time
        TE[i+1]=TE_i+dTE    
    return hw,TE


def Runge_Kutta(hw,TE,dt,timesteps,miu_c,miu_ann,miu0,en,f_ann,f_ran,tao_cor):
    #hw_i and TE_i are used to save hw[i]
    hw_i=0
    TE_i=0
    
    #k1 to k4 multiple dt
    k1dt_hw=0
    k2dt_hw=0
    k3dt_hw=0
    k4dt_hw=0
    k1dt_TE=0
    k2dt_TE=0
    k3dt_TE=0
    k4dt_TE=0
    
    for i in range(timesteps-1):
        W=np.random.uniform(-1,1)
        
        hw_i=hw[i]
        TE_i=TE[i]
        #By the design, miu_c is 0 or miu_ann=0,so we can andd them together
        miu=miu0*(1+miu_ann*(np.cos(2*np.pi*dt*i/tao-5*np.pi/6)))+miu_c
        b=b0*miu
        R=gamma*b-c
        ksi1=f_ann*np.cos(2*np.pi*dt*i/6)+f_ran*W*tao_cor/dt
        ksi2=0
        k1dt_hw=dhw(hw_i,TE_i,dt,b,ksi1)
        k1dt_TE=dTE(hw_i,TE_i,dt,b,R,ksi1,ksi2,en)
        
        hw_i=hw[i]+k1dt_hw/2
        TE_i=TE[i]+k1dt_TE/2
        miu=miu0*(1+miu_ann*(np.cos(2*np.pi*dt*(i+0.5)/tao-5*np.pi/6)))+miu_c
        b=b0*miu
        R=gamma*b-c
        ksi1=f_ann*np.cos(2*np.pi*dt*(i+0.5)/6)+f_ran*W*tao_cor/dt
        ksi2=0
        k2dt_hw=dhw(hw_i,TE_i,dt,b,ksi1)
        k2dt_TE=dTE(hw_i,TE_i,dt,b,R,ksi1,ksi2,en)
        
        hw_i=hw[i]+k2dt_hw/2
        TE_i=TE[i]+k2dt_TE/2
        miu=miu0*(1+miu_ann*(np.cos(2*np.pi*dt*(i+0.5)/tao-5*np.pi/6)))+miu_c
        b=b0*miu
        R=gamma*b-c
        ksi1=f_ann*np.cos(2*np.pi*dt*(i+0.5)/6)+f_ran*W*tao_cor/dt
        ksi2=0
        k3dt_hw=dhw(hw_i,TE_i,dt,b,ksi1)
        k3dt_TE=dTE(hw_i,TE_i,dt,b,R,ksi1,ksi2,en)
        
        hw_i=hw[i]+k3dt_hw
        TE_i=TE[i]+k3dt_TE
        miu=miu0*(1+miu_ann*(np.cos(2*np.pi*dt*(i+1)/tao-5*np.pi/6)))+miu_c
        b=b0*miu
        R=gamma*b-c
        ksi1=f_ann*np.cos(2*np.pi*dt*(i+1)/6)+f_ran*W*tao_cor/dt
        ksi2=0
        k4dt_hw=dhw(hw_i,TE_i,dt,b,ksi1)
        k4dt_TE=dTE(hw_i,TE_i,dt,b,R,ksi1,ksi2,en)
        
        hw[i+1]=hw[i]+(k1dt_hw+2*k2dt_hw+2*k3dt_hw+k4dt_hw)/6
        TE[i+1]=TE[i]+(k1dt_TE+2*k2dt_TE+2*k3dt_TE+k4dt_TE)/6
    return hw,TE
        

        
