# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 05:38:40 2022

@author: Administrator
"""

import matplotlib.pyplot as plt
import numpy as np
from Schemes_Program1 import *
from Parameters import*
#

def main(setup):
#The parameters have the same meaning in other tasks
    timesteps=setup["timesteps"]
    dt=setup["dt"]
    miu_c=setup["miu_c"]
    miu_ann=setup["miu_ann"]
    miu0=setup["miu0"]
    en=setup["en"]
    f_ran=setup["f_ran"]
    f_ann=setup["f_ann"]
    tao_cor=setup["tao_cor"]
    #use five different initial values to show how the parameter change with TE
    hw_changeT=np.zeros((5,timesteps))
    TE_changeT=np.zeros((5,timesteps))
    
    #use five different initial values to show how the parameter change with hw
    hw_changeH=np.zeros((5,timesteps))
    TE_changeH=np.zeros((5,timesteps))
    
    for i in range(5):
        #set initial conditions of different initial TE, and the same hw
        TE_changeT[i,0]=-0.05+0.1*i
        hw_changeT[i,0]=0
        #set initial conditions of different initial hw, and the same TE
        hw_changeH[i,0]=-0.2+0.1*i
        TE_changeH[i,0]=0.15
        
        #Solve the equations with different T
        (hw_changeT[i,:],TE_changeT[i,:])=Runge_Kutta(hw_changeT[i,:],
        TE_changeT[i,:],dt,timesteps,miu_c,miu_ann,miu0,en,f_ann,f_ran,tao_cor)
        #Solve the equations with different T
        (hw_changeH[i,:],TE_changeH[i,:])=Runge_Kutta(hw_changeH[i,:],
        TE_changeH[i,:],dt,timesteps,miu_c,miu_ann,miu0,en,f_ann,f_ran,tao_cor)
    
    #Get the temperature (K) and height (m) in the real world
    TE_changeT_real=TE_changeT*7.5
    TE_changeH_real=TE_changeH*7.5
    hw_changeT_real=hw_changeT*150
    hw_changeH_real=hw_changeH*150
    time_real=np.linspace(0,timesteps*dt,timesteps)*2
    
    #plot
    plt.figure(1)
    for i in range(5):
        plt.plot(time_real,TE_changeT_real[i,:])
    plt.legend(['TE=-1.875K','TE=-0.375K','TE=1.125K','TE=2.625K','TE=4.125K'],fontsize=12,loc='lower left')
    plt.xlabel('time(month)',size=12)
    plt.ylabel('TE(K)',size=12)
    plt.tick_params(labelsize=12)
    plt.savefig('sensitivity of TE.jpg')
    
    plt.figure(2)
    for i in range(5):
        plt.plot(time_real,TE_changeH_real[i,:])
    plt.legend(['hw=-30m','hw=-15m','hw=0m','hw=15m','hw=30m'],fontsize=12,loc='lower left')
    plt.xlabel('time(month)',size=12)
    plt.ylabel('TE(K)',size=12)
    plt.tick_params(labelsize=12)
    plt.savefig('sensitivity of hw.jpg')
    
    plt.figure(3)
    for i in range(5):
        plt.plot(hw_changeT_real[i,:],TE_changeT_real[i,:])
    plt.legend(['TE=-1.875K','TE=-0.375K','TE=1.125K','TE=2.625K','TE=4.125K'],fontsize=12,loc='lower left')
    plt.xlabel('hw(m)',size=12)
    plt.ylabel('TE(K)',size=12)
    plt.tick_params(labelsize=12)
    plt.savefig('trajectories of TE.jpg')
    
    plt.figure(4)
    for i in range(5):
        plt.plot(hw_changeH_real[i,:],TE_changeH_real[i,:])
    plt.legend(['hw=-30m','hw=-15m','hw=0m','hw=15m','hw=30m'],fontsize=12,loc='lower left')
    plt.xlabel('hw(m)',size=12)
    plt.ylabel('TE(K)',size=12)
    plt.tick_params(labelsize=12)
    plt.savefig('trajectories of hw.jpg')

main(setup_final)
 
