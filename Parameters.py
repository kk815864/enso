# -*- coding: utf-8 -*-
"""
Created on Thu Feb  3 23:59:13 2022

@author: Administrator
"""

import numpy as np

#constant parameters:
b0=2.5
gamma=0.75
c=1
r=0.25
alpha=0.125
tao=6 #

# Dictionary of parameters for each setup, parameters can be changed there.
#These are parameters in task A
# They are recommened to be used in task A to C, because you won't change it too much
setup1 = {'en': 0.,   # the non linear parameter
          'miu_c': 2/3,   # Constant miu
          'miu0':0.,  #variable to calculate miu
          'miu_ann': 0., # annual change of miu
          'f_ann': 0.,  # annual change of ksi1
          'f_ran': 0.,     # random change of ksi1
          'tao_cor': 1/60, #tao_cor
          'timesteps':100,
          'dt':1} #the frequency of changing of random term'''

#These are parameters in the most complex condition
# good to be used in task D to G, because you won't change it too much
setup_final = {'en': 0.1,   # the non linear parameter
          'miu_c': 0.,   # Constant miu
          'miu0':0.75,  #variable to calculate miu
          'miu_ann': 0.2, # annual change of miu
          'f_ann': 0.02,  # annual change of ksi1
          'f_ran': 0.2,     # random change of ksi1
          'tao_cor': 1/60,
          'timesteps':6000,
          'dt':1/60} #the frequency of changing of random term'''

