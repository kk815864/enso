# -*- coding: utf-8 -*-
"""
Created on Fri Jan 14 06:12:46 2022

@author: Administrator
"""
import matplotlib.pyplot as plt
import numpy as np
from Schemes_Program1 import *
from Parameters import*

#Initial condition
    timesteps=setup["timesteps"] #how many time steps
    dt=setup["dt"] #length of time step  
    hw=np.zeros(timesteps)#thermoclinic depth, from hw[0] to hw[99]
    TE=np.zeros(timesteps)#east Pacific SST anomaly, from  TE[0] to TE[99]
    hw[0]=0
    TE[0]=0.15
    
    miu_c=setup["miu_c"] #constant coupling coefficient miu
    miu_ann=setup["miu_ann"] #annual cycle of coupling coefficient 
    miu0=setup["miu0"]#another parameter to calculate a variable miu
    en=setup["en"] #the degree of nonlinearity
    f_ran=setup["f_ran"] #annual cycle of wind stress force
    f_ann=setup["f_ann"] #random terms of wind stress force
    tao_cor=setup["tao_cor"] #time interval between changes of stochastic item
    (hw,TE)=Runge_Kutta(hw,TE,dt,timesteps,miu_c,miu_ann,miu0,en,f_ann,f_ran,tao_cor)
    #(hw,TE)=Eular(hw,TE,dt,timesteps)
    
    #Get the temperature (K), time (month), and height (m) in the real world
    hw_real=hw*150
    TE_real=TE*7.5
    time_real=np.linspace(0,timesteps*dt,timesteps)*2
   
    #plot
    plt.subplot(1,3,1)
    plt.plot(hw_real,TE_real,color='blue')
    plt.tick_params(labelsize=12)
    plt.ylabel('TE(K)',size=12)
    plt.xlabel('hw(m)',size=12)
    plt.subplot(1,3,2)
    plt.plot(time_real,hw_real,color='blue')
    plt.tick_params(labelsize=12)
    plt.ylabel('hw(m)',size=12)
    plt.xlabel('time(month)',size=12)
    plt.subplot(1,3,3)
    plt.plot(time_real,TE_real,color='blue')
    plt.tick_params(labelsize=12)
    plt.ylabel('TE(K)',size=12)
    plt.xlabel('time(month)',size=12)
    plt.subplots_adjust(wspace=1.2)
    plt.savefig('ENSO_EULAR.jpg')
    plt.show()
    
main(setupA)

 
